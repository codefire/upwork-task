import com.labracode.Network;

/**
 * Starts application
 */
public class ApplicationStarter {

    public static void main(String[] args) {
        Network network = new Network(2000);

        network.connect(10, 15);
        network.query(90, 100);
        network.query(60, 50);
        network.query(2, 30);
    }

}