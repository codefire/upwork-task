package com.labracode;

import com.labracode.exception.AlreadyConnectedException;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Network implementation.
 * Built on the recursive approach of the "Adjacency list" algorithm.
 *
 * @link https://en.wikipedia.org/wiki/Adjacency_list
 */
public class Network implements NetworkOperations {

    private final Integer size;
    private final Map<Integer, Set<Integer>> map;

    private boolean result = false;

    /**
     * Note about "1" - the max size of each set could be (size - 1), this is due to exclude selfreference!
     *
     * @param size - size of set elements.
     */
    public Network(Integer size) {
        if (size <= 1) {
            throw new IllegalArgumentException("The size number should be bigger than 1!");
        }
        this.size = size;
        this.map = new HashMap<>();
        IntStream.range(1, size + 1).forEach(value -> this.map.put(value, new HashSet<>()));
    }

    @Override
    public void connect(Integer first, Integer second) {
        paramsValidation(first, second);
        if (isConnected(first, second)) {
            throw new AlreadyConnectedException("Elements: [" + first + ", " + second + "] are already connected!");
        }
        this.map.get(first).add(second);
        this.map.get(second).add(first);
    }

    @Override
    public boolean query(Integer first, Integer second) {
        paramsValidation(first, second);
        return isConnected(first, second);
    }

    /**
     * Parameters validation.
     *
     * @param first - first element of connection.
     * @param second - second element of connection.
     */
    private void paramsValidation(Integer first, Integer second) {
        if (first.equals(second)) {
            throw new IllegalArgumentException("Elements should be not equals!");
        }
        if (!hasElement(first)) {
            throw new NoSuchElementException("Element: [" + first + "] not found!");
        }
        if (!hasElement(second)) {
            throw new NoSuchElementException("Element: [" + first + "] not found!");
        }
    }

    private boolean hasElement(Integer element) {
        return this.map.containsKey(element);
    }

    /**
     * Connection checking.
     *
     * @param first - first element of connection.
     * @param second - second element of connection.
     * @return <code>boolean</code> of connection result.
     */
    private boolean isConnected(Integer first, Integer second) {
        Stack<Integer> stackNodes = new Stack<>();
        List<Integer> visitedNodes = new ArrayList<>();
        result = false;
        //pushing root node
        stackNodes.push(first);
        return findElementsConnection(this.map.get(first), first, second, stackNodes, visitedNodes);
    }

    /**
     * Recursive connection checking.
     *
     * @param node - node of the elements, which will be processed.
     * @param first - first element of connection.
     * @param second - second element of connection.
     * @return <code>boolean</code> of connection result.
     */
    private boolean findElementsConnection(Set<Integer> node, Integer first, Integer second, Stack<Integer> stackNodes, List<Integer> visitedNodes) {
        if (node.contains(second)) {
            result = true;
        }

        for (Integer element : node) {
            if (visitedNodes.contains(element)) {
                continue;
            }
            if (!stackNodes.contains(element)) {
                stackNodes.push(element);
                visitedNodes.add(element);
                findElementsConnection(this.map.get(element), first, second, stackNodes, visitedNodes);
            } else {
                Integer previous = stackNodes.pop();
                if (previous.equals(first)) {
                    break;
                }
                findElementsConnection(this.map.get(previous), first, second, stackNodes, visitedNodes);
            }
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        this.map.forEach((k, v) -> {
            builder.append(k);
            builder.append(": [");
            if (v.size() > 0) {
                v.forEach(integer -> {
                    builder.append(integer);
                    builder.append(", ");
                });
                builder.delete(builder.toString().length() - 2, builder.toString().length());
            }
            builder.append("]");
            builder.append("\n");
        });
        return builder.toString();
    }

}