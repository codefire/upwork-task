package com.labracode;

/**
 * API Network operations.
 */
public interface NetworkOperations {

    /**
     * Connecting elements.
     *
     * @param first - first element of connection.
     * @param second - second element of connection.
     */
    void connect(Integer first, Integer second);

    /**
     * Querying of elements connection.
     *
     * @param first - first element of connection.
     * @param second - second element of connection.
     * @return <code>boolean</code> of connection result.
     */
    boolean query(Integer first, Integer second);

}