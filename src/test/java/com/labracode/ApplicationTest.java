package com.labracode;

import com.labracode.exception.AlreadyConnectedException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Application tests.
 */
public class ApplicationTest {

    private final Integer NETWORK_SIZE = 8;
    private Network network;

    @Before
    public void init() {
        network = new Network(NETWORK_SIZE);
        network.connect(1, 2);
        network.connect(6, 2);
        network.connect(2, 4);
        network.connect(5, 8);
    }

    @Test
    public void checkingThatElementsAreConnected() {
        System.out.println("checkingThatElementsAreConnected ...");
        assertTrue(network.query(4, 6));
        assertTrue(network.query(6, 4));
    }

    @Test
    public void checkingThatElementsAreNotConnected() {
        System.out.println("checkingThatElementsAreNotConnected ...");
        assertFalse(network.query(5, 7));
        assertFalse(network.query(7, 5));
    }

    @Test
    public void checkingThatNotConnectedElementsShouldBeConnected() {
        Integer first = 5;
        Integer second = 7;
        System.out.println("checkingThatNotConnectedElementsShouldConnect ...");
        System.out.format("Network before connecting [%d, %d]: \n", first, second);
        System.out.println(network);
        network.connect(first, second);
        System.out.println("Network after connecting:");
        System.out.println(network);
    }

    @Test(expected = AlreadyConnectedException.class)
    public void checkingThatAlreadyConnectedElementsShouldThrowException() {
        System.out.println("checkingThatAlreadyConnectedElementsShouldThrowException ...");
        network.connect(1, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkingThatInvalidNetworkSizeShouldThrowException() {
        network = new Network(-5);
    }

}